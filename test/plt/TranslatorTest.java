package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}

	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithWowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithWowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithMoreConsonants() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainingMoreWordsWithBlankSpaces() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainingCompositeWords() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainingPunctuationsAtEnd() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainingPunctuationsAtBeginAndAtEnd() {
		String inputPhrase = "(hello world)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(ellohay orldway)", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainingFollowingPunctuations() {
		String inputPhrase = "(hello world!)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(ellohay orldway!)", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainingAtLeastOnePunctuationNotAllowed() {
		String inputPhrase = "[hello, world!]";
		Translator translator = new Translator(inputPhrase);
		assertEquals("In the sentence there's at least one punctuation mark that is not allowed",
				translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingAllPunctuations() {
		String inputPhrase = "apple, world!? (utility: well-being' ask; any.)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay, orldway!? (utilitynay: ellway-eingbay' askay; anynay.)", translator.translate());
	}

}
