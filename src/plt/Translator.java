package plt;

public class Translator {

	public static final String NIL = "nil";
	public static final char[] PUNCTSALLOWED = { '.', ',', ';', ':', '?', '!', '\'', '(', ')' };

	private String phrase;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		if (phrase.isEmpty()) {
			return NIL;
		}

		String[] arrayWordsSpace = phrase.split(" ");
		String[] arrayWordsOutput = new String[arrayWordsSpace.length];

		for (int i = 0; i < arrayWordsSpace.length; i++) {
			String firstPuncts = "";

			while (!Character.isLetter(arrayWordsSpace[i].charAt(0))) {
				if (!isAllowed(arrayWordsSpace[i].charAt(0)))
					return "In the sentence there's at least one punctuation mark that is not allowed";

				firstPuncts += arrayWordsSpace[i].charAt(0);
				arrayWordsSpace[i] = arrayWordsSpace[i].substring(1);
			}
			String lastPuncts = "";

			while (!Character.isLetter(arrayWordsSpace[i].charAt(arrayWordsSpace[i].length() - 1))) {
				if (!isAllowed(arrayWordsSpace[i].charAt(arrayWordsSpace[i].length() - 1)))
					return "In the sentence there's at least one punctuation mark that is not allowed";

				lastPuncts = arrayWordsSpace[i].charAt(arrayWordsSpace[i].length() - 1) + lastPuncts;
				arrayWordsSpace[i] = arrayWordsSpace[i].substring(0, arrayWordsSpace[i].length() - 1);
			}

			String[] arrayWordsDash = arrayWordsSpace[i].split("-");
			int dashOccurences = arrayWordsDash.length - 1;

			if (dashOccurences == 0) {
				arrayWordsOutput[i] = checkAndTranslateAWord(arrayWordsSpace[i]);
				arrayWordsOutput[i] = firstPuncts + arrayWordsOutput[i];
				arrayWordsOutput[i] += lastPuncts;
			} else {
				for (int k = 0; k < arrayWordsDash.length; k++) {
					arrayWordsDash[k] = checkAndTranslateAWord(arrayWordsDash[k]);
				}

				arrayWordsOutput[i] = String.join("-", arrayWordsDash);
				arrayWordsOutput[i] = firstPuncts + arrayWordsOutput[i];
				arrayWordsOutput[i] += lastPuncts;
			}
		}

		return String.join(" ", arrayWordsOutput);
	}

	private boolean startWithVowel(String input) {
		return input.startsWith("a") || input.startsWith("e") || input.startsWith("i") || input.startsWith("o")
				|| input.startsWith("u");
	}

	private boolean endWithVowel(String input) {
		return input.endsWith("a") || input.endsWith("e") || input.endsWith("i") || input.endsWith("o")
				|| input.endsWith("u");
	}

	private int getNumberOfStartingConsonants(String input) {
		int count = 0;
		for (int i = 0; i < input.length(); i++) {
			if (isConsonant(input.charAt(i))) {
				count++;
			} else {
				return count;
			}
		}
		return count;
	}

	private boolean isConsonant(char character) {
		return character != 'a' && character != 'e' && character != 'i' && character != 'o' && character != 'u';
	}

	private String checkAndTranslateAWord(String word) {
		if (startWithVowel(word)) {
			if (word.endsWith("y")) {
				return word + "nay";
			} else if (endWithVowel(word)) {
				return word + "yay";
			} else {
				return word + "ay";
			}
		} else {
			String appendedConsonants = word;
			int startingConsonants = getNumberOfStartingConsonants(word);
			for (int i = 0; i < startingConsonants; i++) {
				appendedConsonants += word.charAt(i);
			}
			return appendedConsonants.substring(startingConsonants) + "ay";
		}
	}

	private boolean isAllowed(char ch) {
		return (new String(PUNCTSALLOWED).contains(Character.toString(ch)));
	}

}
